const { Client } = require('pg');

const dbConfig = {
  user: 'postgres',
  host: 'localhost',
  database: 'Arduino',
  password: '9504',
  port: 5432,
};

const client = new Client(dbConfig);

async function executeQueries() {
  try {
    await client.connect();

    // -- 1. What is the percentage of posts that have at least one answer?

    const query1 = `SELECT (COUNT (CASE WHEN answercount > 0 THEN 1 ELSE NULL END) * 100.0)/ 
                        (COUNT(*)) AS percentage_answered FROM posts WHERE posts.postTypeId = 1`;

    // -- 2. List the top 10 users who have the most reputation
    const query2 = `SELECT displayname AS username, reputation
      FROM users
      ORDER BY reputation DESC
      limit 10`;

    //-- 3. Which day of the week has most questions answered within an hour?
    const query3 = `SELECT
        TO_CHAR(answer.creationdate, 'Day') AS day_of_week,
        COUNT(*) AS answered_questions_count
        FROM
            posts question
        INNER JOIN
            posts answer ON question.parentid = answer.id
        WHERE
            question.posttypeid = 2
            AND AGE(question.creationdate, answer.creationdate) <= INTERVAL '1 hour'
        GROUP BY
            day_of_week
        ORDER BY
            answered_questions_count DESC
        LIMIT 1`;

    //-- 4. Find the top 10 posts with the most upvotes in 2015
    const query4 = `SELECT 
        posts.id,COUNT(votetypeid) as upvotes 
        FROM votes
        INNER JOIN posts
        ON posts.id = votes.postid
        WHERE votetypeid = 2 AND EXTRACT(year from posts.creationdate) = 2015
        GROUP BY posts.id
        ORDER BY upvotes desc
        limit 10`;

    // -- 5. Find the top 5 tags associated with the most number of posts

    const query5 = `SELECT tagname, count FROM tags
        ORDER BY count DESC
        LIMIT 5`;

    // -- 6. Find the number of questions asked every year
    const query6 = `SELECT
        EXTRACT(YEAR FROM (creationdate)) AS year,
        COUNT(*) AS question_count
        FROM posts
        WHERE posttypeid = 1
        GROUP BY year
        ORDER BY year`;

    // -- 7. For the questions asked in 2014, find any 3 "rare" questions that are associated with the least used tags
    const query7 = `WITH LeastUsedTags AS (
      SELECT tagname
      FROM tags
      ORDER BY COUNT ASC
        limit 10
      )
      
      SELECT post.*
      FROM posts post
      JOIN tags tag ON post.tags 
      LIKE CONCAT('%', tag.tagname, '%')
      WHERE tag.tagname IN 
      (SELECT tagname FROM LeastUsedTags) 
      and extract(year from post.creationdate) = 2014`;

    // -- 8. When did arduino.stackexchange.com have the most usage? Has it declined in usage now?
    const query8 = `WITH YearlyEngagement AS (
      SELECT
          EXTRACT(YEAR FROM p.creationdate) AS creation_year,
          COUNT(*) AS total_posts,
          SUM(p.commentcount) AS total_comments,
          SUM(p.score) AS total_votes
      FROM posts p
      GROUP BY creation_year
  ),
  TotalEngagements AS (
      SELECT
          ye.creation_year,
          ye.total_posts,
          ye.total_comments,
          ye.total_votes,
          ye.total_posts + ye.total_comments + ye.total_votes AS total_engagements
      FROM YearlyEngagement ye
  )
  SELECT
      te.creation_year,
      te.total_posts,
      te.total_comments,
      te.total_votes,
      te.total_engagements,
      CASE
          WHEN te.creation_year = (SELECT creation_year FROM TotalEngagements ORDER BY total_engagements DESC LIMIT 1)
          THEN 'Most Used Year'
          WHEN te.creation_year = (SELECT creation_year FROM TotalEngagements ORDER BY total_engagements ASC LIMIT 1)
          THEN 'Least Used Year'
          ELSE 'Other Years'
      END AS usage_status
  FROM TotalEngagements te
  ORDER BY total_engagements DESC`;

    // -- 9. Find the top 5 users who have performed the most number of actions in terms of creating posts, comments, and votes.
    const query9 = `SELECT
    u.id AS user_id,
    u.displayname,
    u.upvotes,
    u.downvotes,
    COALESCE(c.comment_count, 0) AS comment_count,
    COALESCE(c.comment_count * 3, 0) AS comment_score,
    COALESCE(p.post_count, 0) AS post_count,
    COALESCE(p.post_count * 10, 0) AS post_score,
    COALESCE(c.comment_count * 3, 0) + u.upvotes + u.downvotes + COALESCE(p.post_count * 10, 0) AS total_score
    FROM users u
    LEFT JOIN (
        SELECT
            userid,
            COUNT(*) AS comment_count
        FROM comments
        GROUP BY userid
    ) c ON u.id = c.userid
    LEFT JOIN (
        SELECT
            owneruserid,
            COUNT(*) AS post_count
        FROM posts
        GROUP BY owneruserid
    ) p ON u.id = p.owneruserid
    ORDER BY total_score DESC
    LIMIT 5`;

    const result1 = await client.query(query1);
    const result2 = await client.query(query2);
    const result3 = await client.query(query3);
    const result4 = await client.query(query4);
    const result5 = await client.query(query5);
    const result6 = await client.query(query6);
    const result7 = await client.query(query7);
    const result8 = await client.query(query8);
    const result9 = await client.query(query9);

    const percentage_answered = Number(
      result1.rows[0].percentage_answered,
    ).toFixed(2);

    // Display the query results
    // task 1
    console.log('Task-1 -> Percentage answered:', percentage_answered);
    // task 2
    console.log('Task-2 -> Answered Posts:', result2.rows);
    // task 3
    console.log(
      'Task-3 -> Most answered question of the day in the week:',
      result3.rows,
    );

    // task 4
    console.log('Task-4 -> Top 10 most voted posts ids: ', result4.rows);

    // task 5
    console.log('Task-5 -> Top 5 most associated tags', result5.rows);

    // task 6
    console.log('Task-6 -> No of questions asked every year', result6.rows);

    // task 7
    console.log('Task-7 -> Three rare questions in 2014: ', result7.rows);

    // task 8
    console.log(
      'task-8 -> Determine when arduino.exchange was most used and is it in decline: ',
      result8.rows,
    );
    console.log(
      'From observing above data one can easily determine it is in DECLINE',
    );

    // task 9
    console.log('task-9 -> Top 5 most active users are: ', result9.rows);

    console.log('All exercises completed!!!');
  } catch (error) {
    console.error('Error executing queries:', error);
  } finally {
    await client.end();
  }
}
executeQueries();
