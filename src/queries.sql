-- 1. What is the percentage of posts that have at least one answer?
SELECT (COUNT (CASE WHEN answercount > 0 THEN 1 ELSE NULL END) * 100.0)/ (COUNT(*)) 
AS percentage_answered
FROM posts
WHERE posts.postTypeId = 1;

-- 2. List the top 10 users who have the most reputation
SELECT displayname AS username, reputation
FROM users
ORDER BY reputation DESC
limit 10;

-- 3. Which day of the week has most questions answered within an hour?
SELECT TO_CHAR(answer.creationdate, 'Day') AS day_of_week,
COUNT(*) AS answered_questions_count
FROM
    posts question
INNER JOIN
    posts answer ON question.parentid = answer.id
WHERE
    question.posttypeid = 2
    AND AGE(question.creationdate, answer.creationdate) <= INTERVAL '1 hour'
GROUP BY
    day_of_week
ORDER BY
    answered_questions_count DESC
LIMIT 1;


-- 4. Find the top 10 posts with the most upvotes in 2015
SELECT 
posts.id,COUNT(votetypeid) as upvotes 
FROM votes
INNER JOIN posts
ON posts.id = votes.postid
WHERE votetypeid = 2 AND EXTRACT(year from posts.creationdate) = 2015
GROUP BY posts.id
ORDER BY upvotes desc
limit 10;

-- 5. Find the top 5 tags associated with the most number of posts

SELECT tagname, count FROM tags
ORDER BY count DESC
LIMIT 5

-- 6. Find the number of questions asked every year
SELECT
EXTRACT(YEAR FROM (creationdate)) AS year,
COUNT(*) AS question_count
FROM posts
WHERE posttypeid = 1
GROUP BY year
ORDER BY year;

-- 7. For the questions asked in 2014, find any 3 "rare" questions that are associated with the least used tags
WITH LeastUsedTags AS (
    SELECT tagname
    FROM tags
    ORDER BY COUNT ASC
    limit 10
    )
    
    SELECT post.*
    FROM posts post
    JOIN tags tag ON post.tags 
    LIKE CONCAT('%', tag.tagname, '%')
    WHERE tag.tagname IN 
    (SELECT tagname FROM LeastUsedTags) 
    and extract(year from post.creationdate) = 2014

-- 8. When did arduino.stackexchange.com have the most usage? Has it declined in usage now?
WITH YearlyEngagement AS (
    SELECT
        EXTRACT(YEAR FROM p.creationdate) AS creation_year,
        COUNT(*) AS total_posts,
        SUM(p.commentcount) AS total_comments,
        SUM(p.score) AS total_votes
    FROM posts p
    GROUP BY creation_year
),
TotalEngagements AS (
    SELECT
        ye.creation_year,
        ye.total_posts,
        ye.total_comments,
        ye.total_votes,
        ye.total_posts + ye.total_comments + ye.total_votes AS total_engagements
    FROM YearlyEngagement ye
)
SELECT
    te.creation_year,
    te.total_posts,
    te.total_comments,
    te.total_votes,
    te.total_engagements,
    CASE
        WHEN te.creation_year = (SELECT creation_year FROM TotalEngagements ORDER BY total_engagements DESC LIMIT 1)
        THEN 'Most Used Year'
        WHEN te.creation_year = (SELECT creation_year FROM TotalEngagements ORDER BY total_engagements ASC LIMIT 1)
        THEN 'Least Used Year'
        ELSE 'Other Years'
    END AS usage_status
FROM TotalEngagements te
ORDER BY total_engagements DESC;


-- 9. Find the top 5 users who have performed the most number of actions in terms of creating posts, comments, and votes.
SELECT
    u.id AS user_id,
    u.displayname,
    u.upvotes,
    u.downvotes,
    COALESCE(c.comment_count, 0) AS comment_count,
    COALESCE(c.comment_count * 3, 0) AS comment_score,
    COALESCE(p.post_count, 0) AS post_count,
    COALESCE(p.post_count * 10, 0) AS post_score,
    COALESCE(c.comment_count * 3, 0) + u.upvotes + u.downvotes + COALESCE(p.post_count * 10, 0) AS total_score
FROM users u
LEFT JOIN (
    SELECT
        userid,
        COUNT(*) AS comment_count
    FROM comments
    GROUP BY userid
) c ON u.id = c.userid
LEFT JOIN (
    SELECT
        owneruserid,
        COUNT(*) AS post_count
    FROM posts
    GROUP BY owneruserid
) p ON u.id = p.owneruserid
ORDER BY total_score DESC
LIMIT 5;


----------- // COMPLETED ALL TASKS // ------------------